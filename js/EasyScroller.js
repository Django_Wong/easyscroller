/* 
* @Author: Django Wong
* @Date:   2015-01-05 11:38:23
* @Last Modified by:   Wong
* @Last Modified time: 2015-01-05 12:27:12
*/

var EasyScroll = function(options){
	"use strict";
	var _ = this;

	_.o = $.extend({
		content: 'noSelector',
		scroll_x: '.scroller-x',
		scroll_y: '.scroller-y',
		handle_class: 'div.handle'
	}, options);

	_.elements = $(_.o.content);
	_.mousedown = false;
	_.target = undefined;
	EasyScroll.prototype.init= function(){
		_.render();
		_.elements.bind('scroll', _.scroll);
		return _;
	};
	EasyScroll.prototype.render = function(){
		_.elements.css('marginRight', function(i,v){
			return parseFloat(v)-40;
		}).css('paddingRight', function(i,v){
			return parseFloat(v)+40;
		}).css('marginBottom', function(i,v){
			return parseFloat(v)-40;
		}).css('paddingBottom', function(i,v){
			return parseFloat(v)+40;
		}).parent().css({'position':'relative', 'overflow':'hidden'});
		_.compute(_.elements);
		_.dragging();

		return _;
	};
	EasyScroll.prototype.scroll = function(e,r) {
		if(_.mousedown === true){
			return;
		}
		var element = _.elements;
		var s = {
			w1:element[0].offsetWidth,
			h1:element[0].offsetHeight,
			w2:element[0].scrollWidth,
			h2:element[0].scrollHeight,
			top:element[0].scrollTop,
			left:element[0].scrollLeft,
			diff_x:0,
			diff_y:0,
			ratio_x:0,
			ratio_y:0
		};
		s.diff_x = s.w2-s.w1;
		s.diff_y = s.h2-s.h1;
		s.ratio_x = s.w1/s.w2;
		s.ratio_y = s.h1/s.h2;
		element.parent().find(_.o.scroll_y).find(_.o.handle_class).css({
			'top': s.top/s.h2*100+'%'
		});
		element.parent().find(_.o.scroll_x).find(_.o.handle_class).css({
			'left': s.left/s.w2*100+'%'
		});
	};
	EasyScroll.prototype.compute = function(element){
		var s = {
			w1:element[0].offsetWidth,
			h1:element[0].offsetHeight,
			w2:element[0].scrollWidth,
			h2:element[0].scrollHeight,
			top:element[0].scrollTop,
			left:element[0].scrollLeft,
			diff_x:0,
			diff_y:0,
			ratio_x:0,
			ratio_y:0
		};
		s.diff_x = s.w2-s.w1;
		s.diff_y = s.h2-s.h1;
		s.ratio_x = s.w1/s.w2;
		s.ratio_y = s.h1/s.h2;
		console.log(s);
		var y = element.parent().find(_.o.scroll_y).find(_.o.handle_class);
		var x = element.parent().find(_.o.scroll_x).find(_.o.handle_class);
		y.css({
			'height': s.ratio_y*100+'%'
		});
		x.css({
			'width': s.ratio_x*100+'%'
		});
		if(y.css('top') !== undefined && y.css('top') !== 'auto'){
			var p_y = parseFloat(y.css('top'))/parseFloat(y.parent(_.o.scroll_y)[0].offsetHeight)*s.h2;
			element.stop(true, true).animate({scrollTop:p_y}, 0);
		}
		if(x.css('left') !== undefined && x.css('left') !== 'auto'){
			var p_x = parseFloat(x.css('left'))/parseFloat(x.parent(_.o.scroll_x)[0].offsetWidth)*s.w2;
			element.stop(true, true).animate({scrollLeft:p_x}, 0);
		}
			
	};
	EasyScroll.prototype.dragging = function() {
		var x = _.elements.parent().find(_.o.scroll_x).find(_.o.handle_class);
		var y = _.elements.parent().find(_.o.scroll_y).find(_.o.handle_class);
		var m_x = 0;
		var m_y = 0;
		var l = 0;
		var t = 0;
		
		var cancel = function(e){
			_.target = undefined;
			_.mousedown = false;
			m_x = 0;
			m_y = 0;
			$(this).data('status', 'mouseup');
			$('body').removeClass('noselect');
		};
		var start_x = function(e){
			_.target = $(this);
			_.current = 'x';
			_.mousedown = true;
			m_x = e.pageX;
			l = $(this).css('left');
			$(this).data('status', 'mousedown');
			$('body').addClass('noselect');
		};
		var start_y = function(e){
			_.target = $(this);
			_.mousedown = true;
			_.current = 'y';
			m_y = e.pageY;
			t = $(this).css('top'); 
			$(this).data('status', 'mousedown');
			$('body').addClass('noselect');
		};
		x.bind('mousedown', start_x);
		y.bind('mousedown', start_y);
		$(window).bind('mouseup', cancel);

		$(window).bind('mousemove', function(evt){
			
			if(_.mousedown){
				if(_.current === 'x'){
					_.target.css({
						'left': function(i,v){
							t = isNaN(parseFloat(t))?0:parseFloat(t);
							var new_v = t+(evt.pageX-m_x);
							return new_v<0?0:new_v;
						}
					});
				}else if(_.current === 'y'){
					_.target.css({
						'top': function(i,v){
							t = isNaN(parseFloat(t))?0:parseFloat(t);
							var new_v = t+(evt.pageY-m_y);
							return new_v<0?0:new_v;
						}
					});
				}
				_.compute(_.elements);
			}
		});
		return _;
	};
	_.init();
	return _;
};